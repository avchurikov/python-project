- Download and install docker-compose



APP/DB:

    - Run docker-compose -f run-app.yml up -d

CI/CD:

    Gtilab, Jenkins:
    - Run docker-compose -f ci-cd.yml up -d
    
    Gtilab:
    1. ${HOST_IP}:8080 -> Add project and code
    2. Create access token and add to jenkins credentials
    3. Create webhook: insert url and secret token from jenkins job
    
    Jenkins:
    1. ${HOST_IP}:8090 -> unlock jenkins: go to docker exec -ti jenkins-ci cat /var/jenkins_home/secrets/initialAdminPassword
    2. Install default plugins and gitlab plugin, docker-pipeline plugin and create user
    3. Create username password credentials for gitlab, harbor.
    4. Add new Node in Jenkins
    5. Add gitlab configuration in global setting. Test connection.
    6. Create pipeline. Set configuration for gitlab into job.
        Pipeline example:
            
            node("Default")
            {
                stage("Clean WS")
                {
                    cleanWs()
                }
                
                stage("Git checkout")
                {
                    checkout changelog: false, 
                            poll: false, 
                            scm: scmGit(branches: [[name: '*/develop']], extensions: [], 
                            userRemoteConfigs: [[credentialsId: 'git-token', url: 'http://192.168.1.4:8080/gitlab-instance-46f66910/python-project.git']])
                }
                
                stage("Run tests")
                {
                    sh "ls -l"
                    sh """
                        python3 -m virtualenv venv &&
                        source venv/bin/activate &&
                        pytest --help &&
                        deactivate
                    """
                }
                
                stage("Build and Push to Registry")
                {
                    gitlabCommitStatus(connection: gitLabConnection(gitLabConnection: 'Gitlab-Connection', jobCredentialId: ''), name: 'Build') 
                    {
                        docker.withRegistry('http://192.168.1.4:80', 'harbor') 
                        {
                            customImage = docker.build("192.168.1.4:80/library/app:latest")
                            customImage.push()
                        }
                    }
                }
            }
        
    

    Harbor:
    1. Download Harbor: wget https://github.com/goharbor/harbor/releases/download/v2.5.5/harbor-offline-installer-v2.5.5.tgz
    2. Extract Harbor
    3. Put harbor.yml config in folder
    4. Click install.sh
    5. Modify /etc/docker/daemon.json, add insecure registry for http connection and restart docker service.
    6. Run: docker-compose -f ./docker-compose.yml up -d
    
