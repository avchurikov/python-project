import json
import logging
import random
import time
import psycopg2

logging.basicConfig(
    level="INFO",
    format="%(asctime)s — %(name)s — %(levelname)s — %(message)s",
)
logger = logging.getLogger(__name__)

def connectToDB():
  db = "grafana"
  host = "postgres"
  port = 5432
  user = "postgres"
  password = "password"
  connection = psycopg2.connect("dbname={0} host={1} port={2} user={3} password={4}".format(db, host, port, user, password))
  return connection

def closeDbConnection(connection):
  connection.close()

def executeSqlRequest(connection, message):
  table = "logs"
  cursor=connection.cursor()
  cursor.execute("INSERT INTO {0} (message) VALUES ({1})".format(table, message))
  connection.commit()
  cursor.close()


if __name__ == "__main__":
    connection = connectToDB()
    try:
        while True:        
            msg = dict()
            for level in range(50):
                (
                    msg[f"bid_{str(level).zfill(2)}"],
                    msg[f"ask_{str(level).zfill(2)}"],
                ) = (
                    random.randrange(1, 100),
                    random.randrange(100, 200),
                )
            msg["stats"] = {
            "sum_bid": sum(v for k, v in msg.items() if "bid" in k),
            "sum_ask": sum(v for k, v in msg.items() if "ask" in k),
            }
            logger.info(f"{json.dumps(msg)}")
            time.sleep(10)
            message = "'{0}'".format(json.dumps(msg))
            executeSqlRequest(connection, message)
    except Exception as exception:
        print("Exception : {0}".format(exception))
    finally:
        closeDbConnection(connection)
    